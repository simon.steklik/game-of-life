<?php

require __DIR__ . '/vendor/autoload.php';

if (count($argv) < 3) {
	exit("Missing arguments, usage: php " . basename(__FILE__) . " <input.xml> <output.xml>\n");
}
$inputFilename = $argv[1];
$outputFilename = $argv[2];

try {
	$schema = \Gol\Serialization\ValidationSchema::get();
} catch (\Exception $e) {
	exit($e->getMessage());
}

$builder = new \Gol\Serialization\ArrayWorldBuilder(new \Gol\Serialization\RandomConflictStrategy());
$serializer = new \Gol\Serialization\XmlSerializer($schema, $builder);
$growthStrategy = new \Gol\Game\DefaultGrowthStrategy();

$gol = new \Gol\GameOfLife($serializer, $growthStrategy);

try {
	$gol->playFile($inputFilename, $outputFilename);
	echo "Done, output saved in $outputFilename\n";
} catch (\Gol\GolException $e) {
	exit($e->getMessage() . "\n");
}