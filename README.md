# Game of Life

This is a variant of [Conway's Game of Life](https://www.youtube.com/watch?v=C2vgICfQawE) with
multiple species where XML file is used as an input. Exact rules and XML format are described 
here: https://github.com/BrandEmbassy/php-test-gol

To use, clone the repository and run (you need [composer](https://getcomposer.org/))

	composer install

The actual script is run with command

	php gol.php <input file> <output file>
	
where `<input file>` is a XML file in correct format and `<output file>` is where 
the resulting XML will be written to (if the file exists, it will be overwritten!). 

Output file has the same format as the input file and describes the resulting game 
configuration (after specified number of iterations has been run).

### Example

Input file `input.xml` contains the following:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<life>
	<world>
		<cells>10</cells>
		<species>2</species>
		<iterations>1</iterations>
	</world>
	<organisms>
		<organism>
			<x_pos>0</x_pos>
			<y_pos>0</y_pos>
			<species>0</species>
		</organism>
		<organism>
			<x_pos>1</x_pos>
			<y_pos>1</y_pos>
			<species>0</species>
		</organism>
		<organism>
			<x_pos>2</x_pos>
			<y_pos>2</y_pos>
			<species>0</species>
		</organism>
	</organisms>
</life>
```

After running

	php gol.php input.xml output.xml

the file `output.xml` will contain the following:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<life>
	<world>
		<cells>10</cells>
		<species>2</species>
		<iterations>1</iterations>
	</world>
	<organisms>
		<organism>
			<x_pos>1</x_pos>
			<y_pos>1</y_pos>
			<species>0</species>
		</organism>
	</organisms>
</life>
```
