<?php
/**
 * User: simon
 */

namespace Gol\Tests\Game;

use Gol\Game\DefaultGrowthStrategy;
use Gol\World\ArrayWorld;
use PHPUnit\Framework\TestCase;

class DefaultGrowthStrategyTest extends TestCase {

	/**
	 * @return array first part in each item is an array of return values of consecutive calls to $world->get(),
	 * second part is expected result
	 */
	public function standardCasesDataProvider() {
		// assumes 3x3 world, testing cell (0,1)
		return [
			[[0, 0, 0, null, null, null], 0], // organism in (0,1) has 2 neighbors - survives
			[[null, 0, 0, 0, null, null], 0], // cell (0,1) is empty with 3 neighbors - new organism is created
			[[null, 0, 0, null, null, null], null], // cell (0,1) is empty with 2 neighbors - remains empty
			[[0, 0, 0, 0, 0, null], null], // organism in (0,1) has 4 neighbors - dies from overcrowding
		];
	}


	/**
	 * @dataProvider standardCasesDataProvider
	 * @param array $getReturns return values of consecutive calls to $world->get()
	 * @param $expectedResult
	 */
	public function testAllCases(array $getReturns, $expectedResult) {
		$world = $this->createMock(ArrayWorld::class);
		$world->method('get')->willReturnOnConsecutiveCalls(...$getReturns);
		$world->method('isOutOfBounds')->willReturnOnConsecutiveCalls(
			true, false, false, true, false, true, false, false);

		$strategy = new DefaultGrowthStrategy();
		$this->assertSame($expectedResult, $strategy->resolveCell($world, 0, 1));
	}

}
