<?php
/**
 * User: simon
 */

namespace Gol\Tests\Game;

use Gol\Game\DefaultGrowthStrategy;
use Gol\Game\Game;
use Gol\World\ArrayWorld;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase {

	public function testStepCallsAndParameters() {
		$world = $this->createMock(ArrayWorld::class);
		$newWorld = $this->createMock(ArrayWorld::class);
		$strategy = $this->createMock(DefaultGrowthStrategy::class);
		$strategy->method('resolveCell')->willReturn(1);

		$expectedSetArgs = [
			[0, 0, 1],
			[0, 1, 1],
			[1, 0, 1],
			[1, 1, 1],
		];

		$world->expects($this->exactly(1))->method('copy')->willReturn($newWorld);
		$world->expects($this->exactly(1))->method('getXSize')->willReturn(2);
		$world->expects($this->exactly(1))->method('getYSize')->willReturn(2);
		$newWorld->expects($this->exactly(4))->method('set')->withConsecutive(...$expectedSetArgs);

		$game = new Game($world, $strategy);
		$this->assertSame($newWorld, $game->step());
	}
}
