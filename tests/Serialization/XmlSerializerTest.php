<?php
/**
 * User: simon
 */

namespace Gol\Tests\Serialization;

use Gol\Serialization\ArrayWorldBuilder;
use Gol\Serialization\DeserializationException;
use Gol\Serialization\RandomConflictStrategy;
use Gol\Serialization\ValidationSchema;
use Gol\Serialization\WorldBuilder;
use Gol\Serialization\XmlSerializer;
use Gol\World\WorldException;
use PHPUnit\Framework\TestCase;

class XmlSerializerTest extends TestCase {

	/** @var  string */
	private $validationSchema;

	/** @var  WorldBuilder */
	private $worldBuilder;

	public function __construct($name = null, array $data = [], $dataName = '') {
		parent::__construct($name, $data, $dataName);
		$this->validationSchema = ValidationSchema::get();
		$this->worldBuilder = $this->createMock(ArrayWorldBuilder::class);
	}

	public function testWorldSerializes() {
		$serializer = new XmlSerializer($this->validationSchema, $this->worldBuilder);
		$worldBuilder = new ArrayWorldBuilder(new RandomConflictStrategy()); // actual worldBuilder
		$world = $worldBuilder->newWorld(2, 2, 1, 1)
			->withOrganism(0, 0, 0)
			->withOrganism(1, 0, 0)
			->build();
		$xml = $serializer->serialize($world);
		$expectedXml = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<life>
  <world>
    <cells>2</cells>
    <species>1</species>
    <iterations>1</iterations>
  </world>
  <organisms>
    <organism>
      <x_pos>0</x_pos>
      <y_pos>0</y_pos>
      <species>0</species>
    </organism>
    <organism>
      <x_pos>1</x_pos>
      <y_pos>0</y_pos>
      <species>0</species>
    </organism>
  </organisms>
</life>

XML;
		$this->assertEquals($expectedXml, $xml);

	}

	public function testValidWorldDeserializes() {
		$inputXml = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<life>
  <world>
    <cells>2</cells>
    <species>1</species>
    <iterations>1</iterations>
  </world>
  <organisms>
    <organism>
      <x_pos>0</x_pos>
      <y_pos>0</y_pos>
      <species>0</species>
    </organism>
    <organism>
      <x_pos>1</x_pos>
      <y_pos>0</y_pos>
      <species>0</species>
    </organism>
  </organisms>
</life>

XML;

		$expectedOrganisms = [
			[0, 0, 0],
			[1, 0, 0],
		];

		$this->worldBuilder->expects($this->exactly(1))->method('newWorld')->with(2, 2, 1, 1);
		$this->worldBuilder->expects($this->exactly(2))->method('withOrganism')->withConsecutive(...$expectedOrganisms);
		$this->worldBuilder->expects($this->exactly(1))->method('build');

		$serializer = new XmlSerializer($this->validationSchema, $this->worldBuilder);
		$serializer->deserialize($inputXml);
	}

	public function testInvalidXmlFails() {
		$invalidXml = '<life</life>'; // just something that's not valid xml
		$this->expectException(DeserializationException::class);
		$serializer = new XmlSerializer($this->validationSchema, $this->worldBuilder);
		$serializer->deserialize($invalidXml);
	}

	public function invalidSchemaDataProvider() {
		return [
			['<wrong></wrong>'], // completely wrong schema

			['<life>
				<world>
					<cells>2</cells>
					<iterations>1</iterations>
			  	</world>
			  <organisms>
				<organism>
				  <y_pos>0</y_pos>
				  <species>0</species>
				</organism>
			  </organisms>
			</life>'], // missing elements ("species", "x_pos")

			['<life>
				<world>
					<cells>2</cells>
					<species>-1</species>
					<iterations>1</iterations>
			  	</world>
			  <organisms>
				<organism>
				  <x_pos>abc</x_pos>
				  <y_pos>0</y_pos>
				  <species>0</species>
				</organism>
			  </organisms>
			</life>'] // invalid values ("species", "x_pos")
		];
	}

	/**
	 * @dataProvider invalidSchemaDataProvider
	 */
	public function testValidXmlWithWrongSchemaFails($xml) {
		$this->expectException(DeserializationException::class);
		$serializer = new XmlSerializer($this->validationSchema, $this->worldBuilder);
		$serializer->deserialize($xml);
	}

	public function testDeserializationFailsOnWorldError() {
		$xml = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<life>
  <world>
    <cells>2</cells>
    <species>1</species>
    <iterations>1</iterations>
  </world>
  <organisms>
    <organism>
      <x_pos>0</x_pos>
      <y_pos>0</y_pos>
      <species>2</species>
    </organism>
  </organisms>
</life>

XML;
		$this->expectException(DeserializationException::class);
		$this->worldBuilder->method('withOrganism')->willThrowException(new WorldException());
		$serializer = new XmlSerializer($this->validationSchema, $this->worldBuilder);
		$serializer->deserialize($xml);
	}


}
