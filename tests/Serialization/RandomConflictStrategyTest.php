<?php
/**
 * User: simon
 */

namespace Gol\Tests\Serialization;

use Gol\Serialization\RandomConflictStrategy;
use PHPUnit\Framework\TestCase;

class RandomConflictStrategyTest extends TestCase {

	public function testReturnsOneOfPassedValues() {
		$species = [1, 2, 3];

		$strategy = new RandomConflictStrategy();
		$this->assertContains($strategy->resolveConflict($species), $species);
	}

	public function testReturnsNullOnEmptyArray() {
		$strategy = new RandomConflictStrategy();
		$this->assertEquals(null, $strategy->resolveConflict([]));
	}
}
