<?php
/**
 * User: simon
 */

namespace Gol\Tests\Serialization;

use Gol\Serialization\ArrayWorldBuilder;
use Gol\Serialization\ConflictStrategy;
use Gol\Serialization\RandomConflictStrategy;
use PHPUnit\Framework\TestCase;

class ArrayWorldBuilderTest extends TestCase {

	/** @var  ConflictStrategy */
	private $conflictStrategy;

	public function __construct($name = null, array $data = [], $dataName = '') {
		parent::__construct($name, $data, $dataName);
		$this->conflictStrategy = $this->createMock(RandomConflictStrategy::class);
	}


	public function testEmptyWorldBuilds() {
		$builder = new ArrayWorldBuilder($this->conflictStrategy);
		$world = $builder->newWorld(2, 2, 1, 1)->build();

		$this->assertEquals(2, $world->getXSize());
		$this->assertEquals(2, $world->getYSize());
		$this->assertEquals(1, $world->getSpeciesCount());
		$this->assertEquals(1, $world->getIterationsCount());
	}

	public function testOrganismIsAdded() {
		$builder = new ArrayWorldBuilder($this->conflictStrategy);
		$world = $builder->newWorld(2, 2, 1, 1)
			->withOrganism(0, 0, 0)
			->build();

		$this->assertEquals(0, $world->get(0, 0));
	}

	public function testAddingOrganismFailsWithoutWorld() {
		$this->expectException(\Exception::class);
		$builder = new ArrayWorldBuilder($this->conflictStrategy);
		$builder->withOrganism(0, 0, 0);
	}

	public function testBuildingFailsWithoutWorld() {
		$this->expectException(\Exception::class);
		$builder = new ArrayWorldBuilder($this->conflictStrategy);
		$builder->build();
	}
}
