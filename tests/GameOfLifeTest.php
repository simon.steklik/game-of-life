<?php
/**
 * User: simon
 */

namespace Gol\Testing;

use Gol\Game\DefaultGrowthStrategy;
use Gol\Game\GrowthStrategy;
use Gol\GameOfLife;
use Gol\GolException;
use Gol\Serialization\DeserializationException;
use Gol\Serialization\Serializer;
use Gol\Serialization\XmlSerializer;
use Gol\World\ArrayWorld;
use PHPUnit\Framework\TestCase;

class GameOfLifeTest extends TestCase {

	/** @var  Serializer */
	private $serializer;

	/** @var  GrowthStrategy */
	private $growthStrategy;

	public function __construct($name = null, array $data = [], $dataName = '') {
		parent::__construct($name, $data, $dataName);
		$this->serializer = $this->createMock(XmlSerializer::class);
		$this->growthStrategy = $this->createMock(DefaultGrowthStrategy::class);
	}


	public function testPlayFailsOnDeserializationError() {
		$this->expectException(GolException::class);
		$this->serializer->method('deserialize')->willThrowException(new DeserializationException());
		$gol = new GameOfLife($this->serializer, $this->growthStrategy);
		$gol->play('<xml><element></element></xml>'); // actual xml is not important here
	}

	public function testPlayFileFailsOnEmptyInputFilename() {
		$this->expectException(GolException::class);
		$gol = new GameOfLife($this->serializer, $this->growthStrategy);
		$gol->playFile('', 'output.xml');
	}

	public function testPlayFileFailsOnEmptyOutputFilename() {
		$this->expectException(GolException::class);
		$gol = new GameOfLife($this->serializer, $this->growthStrategy);
		$gol->playFile('input.xml', '');
	}

	public function testPlayFileFailsWhenInputFileDoesNotExist() {
		$this->expectException(GolException::class);
		$gol = new GameOfLife($this->serializer, $this->growthStrategy);
		$gol->playFile('some_nonexistent_file_lskgjndfklgh.xml', 'output.xml');
	}
}
