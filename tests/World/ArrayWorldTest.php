<?php
/**
 * User: simon
 */

namespace Gol\Testing\World;

use Gol\World\ArrayWorld;
use Gol\World\InvalidSpeciesException;
use Gol\World\OutOfBoundsException;
use PHPUnit\Framework\TestCase;

class ArrayWorldTest extends TestCase {

	public function testInitializationWorks() {
		$world = new ArrayWorld(2, 2, 1, 1);
		$this->assertEquals(2, $world->getXSize());
		$this->assertEquals(2, $world->getYSize());
		$this->assertEquals(1, $world->getSpeciesCount());
		$this->assertEquals(1, $world->getIterationsCount());
	}

	public function testBoundsAreRecognized() {
		$world = new ArrayWorld(2, 2, 1, 1);
		$this->assertEquals(true, $world->isOutOfBounds(-1, 0));
		$this->assertEquals(true, $world->isOutOfBounds(0, -1));
		$this->assertEquals(true, $world->isOutOfBounds(2, 0));
		$this->assertEquals(true, $world->isOutOfBounds(0, 2));
		$this->assertEquals(false, $world->isOutOfBounds(0, 0));
		$this->assertEquals(false, $world->isOutOfBounds(1, 1));
	}

	public function testValidOrganismAddedAndFound() {
		$world = new ArrayWorld(2, 2, 1, 1);
		$this->assertSame(null, $world->get(0, 0));
		$world->set(0, 0, 0);
		$this->assertSame(0, $world->get(0, 0));
	}

	public function testAddingOutOfBoundsOrganismFails() {
		$this->expectException(OutOfBoundsException::class);
		$world = new ArrayWorld(2, 2, 1, 1);
		$world->set(2, 2, 0); // world is 2x2
	}

	public function testAddingInvalidSpeciesFails() {
		$this->expectException(InvalidSpeciesException::class);
		$world = new ArrayWorld(2, 2, 1, 1);
		$world->set(0, 0, 1); // world has only 1 species (indexed as 0), so this should fail
	}

	public function testGettingOutOfBoundsOrganismFails() {
		$this->expectException(OutOfBoundsException::class);
		$world = new ArrayWorld(2, 2, 1, 1);
		$world->get(2,2);
	}

	public function testCopyIsValid() {
		$world = new ArrayWorld(2, 2, 2, 1);
		$world->set(0, 0, 0);
		$copy = $world->copy();
		$world->set(0, 0, 1);

		$this->assertEquals(2, $copy->getXSize());
		$this->assertEquals(2, $copy->getYSize());
		$this->assertEquals(2, $copy->getSpeciesCount());
		$this->assertEquals(1, $copy->getIterationsCount());
		$this->assertSame(0, $copy->get(0, 0)); // check that the copy has the organism and was not affected by change in the original world
	}
}
