<?php
/**
 * User: simon
 */

namespace Gol\Tests;

use Gol\Game\DefaultGrowthStrategy;
use Gol\Game\GrowthStrategy;
use Gol\GameOfLife;
use Gol\GolException;
use Gol\Serialization\ArrayWorldBuilder;
use Gol\Serialization\RandomConflictStrategy;
use Gol\Serialization\Serializer;
use Gol\Serialization\ValidationSchema;
use Gol\Serialization\XmlSerializer;
use PHPUnit\Framework\TestCase;

/**
 * Class FunctionalTest tests GameOfLife as a whole (it's not a unit test)
 */
class FunctionalTest extends TestCase {

	/** @var  Serializer */
	private $serializer;

	/** @var  GrowthStrategy */
	private $growthStrategy;

	public function __construct($name = null, array $data = [], $dataName = '') {
		parent::__construct($name, $data, $dataName);
		$validationSchema = ValidationSchema::get();
		$this->serializer = new XmlSerializer($validationSchema, new ArrayWorldBuilder(new RandomConflictStrategy()));
		$this->growthStrategy = new DefaultGrowthStrategy();
	}


	public function testPlayFailsWithInvalidXml() {
		$this->expectException(GolException::class);
		$gol = new GameOfLife($this->serializer, $this->growthStrategy);
		$gol->play('<wrongXml></wrongXml>');
	}

	public function testPlayFailsAfterValidationWithIncorrectOrganisms() {
		$this->expectException(GolException::class);
		// organism has invalid species (only 0 and 1 are allowed for a 2-species world)
		$xml = $this->createValidXml(2, 2, 1, [0 => [0 => 5]]);

		$gol = new GameOfLife($this->serializer, $this->growthStrategy);
		$gol->play($xml);
	}

	/**
	 * @return array with each item as [[cells, species, iterations], [array of organisms], resulting_xml]
	 */
	public function validWorldsDataProvider() {
		return [
			[ // empty world
				[1, 1, 2],
				[],
				[],
			],
			[ // diagonally across 5x5 world
				[5, 1, 2],
				[[0 => 0], [1 => 0], [2 => 0], [3 => 0], [4 => 0]],
				[2 => [2 => 0]],
			],
			[ // more complex situation
				[3, 2, 1],
				[[0 => 0, 1 => 0], [0 => 0, 2 => 1], [1 => 1]],
				[[0 => 0, 1 => 0], [0 => 0, 1 => 0]]
			],
		];
	}


	/**
	 * @dataProvider validWorldsDataProvider
	 * @param array $worldSettings
	 * @param array $startingOrganisms
	 * @param array $finalOrganisms
	 */
	public function testPlayProducesCorrectResult(array $worldSettings, array $startingOrganisms, array $finalOrganisms) {
		$inputXml = $this->createValidXml($worldSettings[0], $worldSettings[1], $worldSettings[2], $startingOrganisms);
		$expectedOutput = $this->createValidXml($worldSettings[0], $worldSettings[1], $worldSettings[2], $finalOrganisms);
		$gol = new GameOfLife($this->serializer, $this->growthStrategy);
		$output = $gol->play($inputXml);

		$this->assertEquals($expectedOutput, $output);
	}

	public function testFilePlaySavesCorrectResultInFile() {
		$gol = new GameOfLife($this->serializer, $this->growthStrategy);
		$inputFilename = __DIR__ . '/resources/testInput.xml';
		$outputFilename = __DIR__ . '/resources/output.xml';
		$expectedOutputFilename = __DIR__ . '/resources/testOutput.xml';
		$gol->playFile($inputFilename, $outputFilename);
		$this->assertXmlFileEqualsXmlFile($outputFilename, $expectedOutputFilename);
	}


	private function createValidXml($size, $species, $iterations, array $organisms) {
		$organismsXml = '';
		foreach ($organisms as $x => $column) {
			foreach ($column as $y => $cell) {
				$organismsXml .= sprintf("<organism><x_pos>%d</x_pos><y_pos>%d</y_pos><species>%d</species></organism>", $x, $y, $cell);
			}
		}
		$xml = sprintf('<?xml version="1.0" encoding="UTF-8"?>
			<life>
			  <world>
				<cells>%d</cells>
				<species>%d</species>
				<iterations>%d</iterations>
			  </world>
			  <organisms>%s</organisms>
			</life>', $size, $species, $iterations, $organismsXml);

		$doc = new \DOMDocument();
		$doc->formatOutput = true;
		$doc->preserveWhiteSpace = false;
		$success = $doc->loadXML($xml);
		if ($success === false) {
			throw new \Exception("createValidXml failed");
		}
		return $doc->saveXML(null, LIBXML_NOEMPTYTAG);
	}
}
