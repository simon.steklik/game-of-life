<?php
/**
 * User: simon
 */

namespace Gol\World;

/**
 * Class OutOfBoundsException is thrown when coordinates outside of World's bounds are accessed
 */
class OutOfBoundsException extends WorldException {}