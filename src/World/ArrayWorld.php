<?php
/**
 * User: simon
 */

namespace Gol\World;

/**
 * Class ArrayWorld is a World with cells implemented as a (sparse) 2d array
 */
class ArrayWorld implements World {

	/** @var  int */
	private $xSize;
	/** @var  int */
	private $ySize;

	/** @var  int */
	private $speciesCount;

	/** @var  int */
	private $iterationsCount;

	/** @var array */
	private $cells;

	/**
	 * ArrayWorld constructor.
	 * @param int $xSize
	 * @param int $ySize
	 * @param int $speciesCount
	 * @param int $iterationsCount
	 * @param array $cells
	 */
	public function __construct($xSize, $ySize, $speciesCount, $iterationsCount, array $cells = []) {
		$this->xSize = $xSize;
		$this->ySize = $ySize;
		$this->speciesCount = $speciesCount;
		$this->iterationsCount = $iterationsCount;
		$this->cells = $cells;
	}


	/**
	 * Returns a species on specified coordinates or null if cell is empty
	 * @param int $x
	 * @param int $y
	 * @return int|null
	 * @throws OutOfBoundsException
	 */
	public function get($x, $y) {
		if ($this->isOutOfBounds($x, $y)) {
			throw new OutOfBoundsException("Out of bounds: [$x, $y]");
		}
		return isset($this->cells[$x][$y]) ? $this->cells[$x][$y] : null;
	}

	/**
	 * Assigns a species to a cell
	 * @param int $x
	 * @param int $y
	 * @param int|null $species
	 * @throws InvalidSpeciesException
	 * @throws OutOfBoundsException
	 */
	public function set($x, $y, $species) {
		if ($species !== null && !$this->isValidSpecies($species)) {
			throw new InvalidSpeciesException("$species is not a valid species");
		}
		if ($this->isOutOfBounds($x, $y)) {
			throw new OutOfBoundsException("Out of bounds: [$x, $y]");
		}
		if ($species === null) {
			unset($this->cells[$x][$y]);
		} else {
			$this->cells[$x][$y] = $species;
		}
	}

	/**
	 * Checks if specified coordinates are out of bounds
	 * @param int $x
	 * @param int $y
	 * @return bool
	 */
	public function isOutOfBounds($x, $y) {
		return $x < 0 || $x > ($this->xSize - 1) || $y < 0 || $y > ($this->ySize - 1);
	}

	/**
	 * @param int $species
	 * @return bool
	 */
	private function isValidSpecies($species) {
		return is_int($species) && $species >= 0 && $species < $this->speciesCount;
	}

	/**
	 * Returns horizontal size of the World
	 * @return int
	 */
	public function getXSize() {
		return $this->xSize;
	}

	/**
	 * Returns vertical size of the World
	 * @return int
	 */
	public function getYSize() {
		return $this->ySize;
	}

	/**
	 * Returns number of iterations
	 * @return int
	 */
	public function getIterationsCount() {
		return $this->iterationsCount;
	}

	/**
	 * Returns number of species that can "live" in this World
	 * @return int
	 */
	public function getSpeciesCount() {
		return $this->speciesCount;
	}

	/**
	 * Returns a (deep) copy of itself
	 * @return ArrayWorld
	 */
	public function copy() {
		return new ArrayWorld($this->xSize, $this->ySize, $this->speciesCount, $this->iterationsCount, $this->cells);
	}

	/**
	 * @return string
	 */
	public function __toString() {
		$string = '';
		for ($y = 0; $y < $this->ySize; $y++) {
			for ($x = 0; $x < $this->xSize; $x++) {
				$string .= isset($this->cells[$x][$y]) ? $this->cells[$x][$y] : '.';
			}
			$string .= "\n";
		}
		return $string;
	}
}