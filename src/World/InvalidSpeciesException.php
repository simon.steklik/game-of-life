<?php
/**
 * User: simon
 */

namespace Gol\World;

/**
 * Class InvalidSpeciesException is thrown when an invalid species is encountered
 */
class InvalidSpeciesException extends WorldException {}