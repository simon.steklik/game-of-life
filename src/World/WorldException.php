<?php
/**
 * User: simon
 */

namespace Gol\World;

/**
 * Class WorldException represents a general World related error
 */
class WorldException extends \Exception {}