<?php
/**
 * User: simon
 */

namespace Gol\World;

/**
 * Interface World represents 2d environment with cells addressable by x,y coordinates
 */
interface World {

	/**
	 * Returns a species on specified coordinates or null if cell is empty
	 * @param int $x
	 * @param int $y
	 * @return int|null
	 */
	public function get($x, $y);

	/**
	 * Assigns a species to a cell
	 * @param int $x
	 * @param int $y
	 * @param int $species
	 */
	public function set($x, $y, $species);

	/**
	 * Checks if specified coordinates are out of bounds
	 * @param int $x
	 * @param int $y
	 * @return bool
	 */
	public function isOutOfBounds($x, $y);

	/**
	 * Returns horizontal size of the World
	 * @return int
	 */
	public function getXSize();

	/**
	 * Returns vertical size of the World
	 * @return int
	 */
	public function getYSize();

	/**
	 * Returns number of species that can "live" in this World
	 * @return int
	 */
	public function getSpeciesCount();

	/**
	 * Returns number of iterations
	 * @return int
	 */
	public function getIterationsCount();

	/**
	 * Returns a (deep) copy of itself
	 * @return World
	 */
	public function copy();
}