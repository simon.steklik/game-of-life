<?php
/**
 * User: simon
 */

namespace Gol\Game;

use Gol\World\World;

/**
 * Class DefaultGrowthStrategy resolves a cell based on its neighbors and rules specified here:
 * https://github.com/BrandEmbassy/php-test-gol
 */
class DefaultGrowthStrategy implements GrowthStrategy {

	/**
	 * Resolves a cell based on its neighbors
	 * @param World $world
	 * @param int $x
	 * @param int $y
	 * @return int|null
	 */
	public function resolveCell(World $world, $x, $y) {
		$currentSpecies = $world->get($x, $y);
		$counts = [];
		for ($i = $x - 1; $i <= $x + 1; $i++) {
			for ($j = $y - 1; $j <= $y + 1; $j++) {
				if ([$i, $j] !== [$x, $y] && !$world->isOutOfBounds($i, $j)) {
					$species = $world->get($i, $j);
					if ($species === null) {
						continue;
					}
					$counts[$species] = isset($counts[$species]) ? $counts[$species] + 1 : 1;
				}
			}
		}

		if ($currentSpecies !== null) {
			if (isset($counts[$currentSpecies]) && ($counts[$currentSpecies] === 2 || $counts[$currentSpecies] === 3)) {
				$finalSpecies = $currentSpecies; // current species survives
			} else {
				$finalSpecies = null; // current species dies
			}
		} else {
			$possibleSpecies = [];
			foreach ($counts as $species => $count) {
				if ($count === 3) {
					$possibleSpecies[] = $species;
				}
			}
			if (count($possibleSpecies) === 0) {
				$finalSpecies = null; // no species can reproduce
			} else {
				$finalSpecies = $possibleSpecies[rand(0, count($possibleSpecies) - 1)]; // pick one at random
			}
		}
		return $finalSpecies;
	}
}