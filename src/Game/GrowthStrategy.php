<?php
/**
 * User: simon
 */

namespace Gol\Game;

use Gol\World\World;

/**
 * Interface GrowthStrategy represents logic that resolves value of a cell based on its position in a World
 */
interface GrowthStrategy {

	/**
	 * Resolves value of a cell based on its position in a World
	 * @param World $world
	 * @param int $x
	 * @param int $y
	 * @return int|null
	 */
	public function resolveCell(World $world, $x, $y);
}