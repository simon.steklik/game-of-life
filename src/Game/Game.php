<?php
/**
 * User: simon
 */

namespace Gol\Game;

use Gol\World\World;

/**
 * Class Game represents the game itself - the world and the way it's updated with each step (iteration)
 */
class Game {

	/** @var  World */
	private $world;

	/** @var  GrowthStrategy */
	private $growthStrategy;

	/**
	 * Game constructor.
	 * @param World $world
	 * @param GrowthStrategy $growthStrategy
	 */
	public function __construct(World $world, GrowthStrategy $growthStrategy) {
		$this->world = $world;
		$this->growthStrategy = $growthStrategy;
	}

	/**
	 * Runs one step (iteration) of the game
	 * @return World
	 */
	public function step() {
		$newWorld = $this->world->copy();
		$xSize = $this->world->getXSize();
		$ySize = $this->world->getYSize();
		for ($x = 0; $x < $xSize; $x++) {
			for ($y = 0; $y < $ySize; $y++) {
				$newWorld->set($x, $y, $this->growthStrategy->resolveCell($this->world, $x, $y));
			}
		}
		$this->world = $newWorld;
		return $newWorld;
	}

	/**
	 * @return World
	 */
	public function getWorld() {
		return $this->world;
	}

}