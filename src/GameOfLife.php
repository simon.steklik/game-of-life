<?php
/**
 * User: simon
 */

namespace Gol;


use Gol\Game\Game;
use Gol\Game\GrowthStrategy;
use Gol\Serialization\DeserializationException;
use Gol\Serialization\Serializer;

/**
 * Class GameOfLife encapsulates all logic for playing Conway's Game of Life
 */
class GameOfLife {

	/** @var  Serializer */
	private $serializer;

	/** @var  GrowthStrategy */
	private $growthStrategy;

	/**
	 * GameOfLife constructor.
	 * @param Serializer $serializer
	 * @param GrowthStrategy $growthStrategy
	 */
	public function __construct(Serializer $serializer, GrowthStrategy $growthStrategy) {
		$this->serializer = $serializer;
		$this->growthStrategy = $growthStrategy;
	}

	/**
	 * Plays the game specified in provided xml
	 * @param string $xml
	 * @return string resulting xml (same format as input)
	 * @throws GolException
	 */
	public function play($xml) {
		try {
			$world = $this->serializer->deserialize($xml);
		} catch (DeserializationException $de) {
			throw new GolException("Error when deserializing world: " . $de->getMessage());
		}

		$game = new Game($world, $this->growthStrategy);
		$iterations = $world->getIterationsCount();

		for ($i = 0; $i < $iterations; $i++) {
			$game->step();
		}

		$finalWorld = $game->getWorld();
		return $this->serializer->serialize($finalWorld);
	}


	/**
	 * Plays the game specified in inputFilename (xml file) and saved the resulting xml as outputFilename
	 * @param string $inputFilename
	 * @param string $outputFilename
	 * @throws GolException
	 */
	public function playFile($inputFilename, $outputFilename) {
		if (empty($inputFilename) || empty($outputFilename)) {
			throw new GolException("Input and output filename cannot be empty");
		}

		$xml = $this->readInputFile($inputFilename);
		$resultXml = $this->play($xml);
		$this->writeOutputFile($outputFilename, $resultXml);
	}

	/**
	 * @param string $filename
	 * @return bool|string
	 * @throws GolException
	 */
	private function readInputFile($filename) {
		$fileContent = @file_get_contents($filename);
		if ($fileContent === false) {
			$error = error_get_last();
			throw new GolException("Cannot read from file, "
				. ($error === null ? 'unknown error' : $error['message']));
		}
		return $fileContent;
	}

	/**
	 * @param string $filename
	 * @param string $content
	 * @throws GolException
	 */
	private function writeOutputFile($filename, $content) {
		$bytesWritten = @file_put_contents($filename, $content);
		if ($bytesWritten === false) {
			$error = error_get_last();
			throw new GolException("Cannot write to file, "
				. ($error === null ? 'unknown error' : $error['message']));
		}
	}
}