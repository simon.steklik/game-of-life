<?php
/**
 * User: simon
 */

namespace Gol\Serialization;

use Gol\World\ArrayWorld;

/**
 * Class ArrayWorldBuilder is used for building ArrayWorld instances
 * @package Gol\Serialization
 */
class ArrayWorldBuilder implements WorldBuilder {

	/** @var  ArrayWorld */
	private $world;

	/** @var  ConflictStrategy */
	private $conflictStrategy;

	/**
	 * ArrayWorldBuilder constructor.
	 * @param ConflictStrategy $conflictStrategy
	 */
	public function __construct(ConflictStrategy $conflictStrategy) {
		$this->conflictStrategy = $conflictStrategy;
	}


	/**
	 * Starts creating a new ArrayWorld instance
	 * @param int $xSize
	 * @param int $ySize
	 * @param int $speciesCount
	 * @param int $iterationsCount
	 * @return $this
	 */
	public function newWorld($xSize, $ySize, $speciesCount, $iterationsCount) {
		$this->world = new ArrayWorld($xSize, $ySize, $speciesCount, $iterationsCount);
		return $this;
	}

	/**
	 * Adds organism to current ArrayWorld instance
	 * @param int $x
	 * @param int $y
	 * @param int $species
	 * @return $this
	 * @throws \Exception
	 */
	public function withOrganism($x, $y, $species) {
		if ($this->world === null)
			throw new \Exception("World not instantiated");

		$presentSpecies = $this->world->get($x, $y);
		if ($presentSpecies !== null) {
			$species = $this->conflictStrategy->resolveConflict([$species, $presentSpecies]);
		}
		$this->world->set($x, $y, $species);
		return $this;
	}

	/**
	 * Returns finished ArrayWorld instance
	 * @return ArrayWorld
	 * @throws \Exception
	 */
	public function build() {
		if ($this->world === null)
			throw new \Exception("World not instantiated");

		return $this->world;
	}

}