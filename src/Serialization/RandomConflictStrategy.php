<?php
/**
 * User: simon
 */

namespace Gol\Serialization;

/**
 * Class RandomConflictStrategy resolves conflicts randomly
 */
class RandomConflictStrategy implements ConflictStrategy {

	/**
	 * Randomly chooses one of specified species
	 * @param int[] $species
	 * @return int|null
	 */
	public function resolveConflict(array $species) {
		if (count($species) === 0)
			return null;
		return $species[rand(0, count($species) - 1)];
	}

}