<?php
/**
 * User: simon
 */

namespace Gol\Serialization;

use Gol\World\World;

/**
 * Interface WorldBuilder represents a builder for World instances
 * @package Gol\Serialization
 */
interface WorldBuilder {

	/**
	 * Starts creating a new World instance
	 * @param int $xSize
	 * @param int $ySize
	 * @param int $speciesCount
	 * @param int $iterationsCount
	 * @return WorldBuilder
	 */
	public function newWorld($xSize, $ySize, $speciesCount, $iterationsCount);

	/**
	 * Adds organism to current World instance
	 * @param int $x
	 * @param int $y
	 * @param int $species
	 * @return WorldBuilder
	 */
	public function withOrganism($x, $y, $species);


	/**
	 * Returns finished World instance
	 * @return World
	 */
	public function build();
}