<?php
/**
 * User: simon
 */

namespace Gol\Serialization;


use Gol\World\World;
use Gol\World\WorldException;

/**
 * Class XmlSerializer is used for serializing and deserializing World instances to/from xml
 */
class XmlSerializer implements Serializer {

	/** @var  string */
	private $schema;

	/** @var  WorldBuilder */
	private $builder;

	/**
	 * XmlSerializer constructor.
	 * @param string $schema
	 * @param WorldBuilder $builder
	 */
	public function __construct($schema, WorldBuilder $builder) {
		$this->schema = $schema;
		$this->builder = $builder;
	}

	/**
	 * @param World $world
	 * @return string
	 */
	public function serialize(World $world) {
		$xml = new \DOMDocument();
		$xml->loadXML('<?xml version="1.0" encoding="UTF-8"?><life><world></world><organisms></organisms></life>');
		$xmlWorld = $xml->getElementsByTagName('world')->item(0);

		$xmlWorld->appendChild($xml->createElement('cells', $world->getXSize()));
		$xmlWorld->appendChild($xml->createElement('species', $world->getSpeciesCount()));
		$xmlWorld->appendChild($xml->createElement('iterations', $world->getIterationsCount()));

		$organisms = $xml->getElementsByTagName('organisms')->item(0);

		for ($x = 0; $x < $world->getXSize(); $x++) {
			for ($y = 0; $y < $world->getYSize(); $y++) {
				$species = $world->get($x, $y);
				if ($species !== null) {
					$organism = $organisms->appendChild($xml->createElement('organism'));
					$organism->appendChild($xml->createElement('x_pos', $x));
					$organism->appendChild($xml->createElement('y_pos', $y));
					$organism->appendChild($xml->createElement('species', $species));
				}
			}
		}
		$xml->formatOutput = true;
		return $xml->saveXML(null, LIBXML_NOEMPTYTAG);
	}

	/**
	 * @param string $xmlString
	 * @return World
	 */
	public function deserialize($xmlString) {
		$xml = $this->load($xmlString);
		$this->validate($xml);
		return $this->createWorld($xml);
	}

	/**
	 * @param string $xmlString
	 * @return \DOMDocument
	 */
	private function load($xmlString) {
		libxml_use_internal_errors(true);
		$xml = new \DOMDocument();
		$isValidXml = $xml->loadXML($xmlString);
		if (!$isValidXml) {
			$errors = libxml_get_errors();
			libxml_clear_errors();
			throw new DeserializationException("XML is not valid: " . $this->getErrorMessage($errors));
		}

		return $xml;
	}

	/**
	 * @param \DOMDocument $xml
	 */
	private function validate(\DOMDocument $xml) {
		libxml_use_internal_errors(true);
		$matchesSchema = $xml->schemaValidateSource($this->schema);
		if (!$matchesSchema) {
			$errors = libxml_get_errors();
			libxml_clear_errors();
			throw new DeserializationException("XML does not match schema: " . $this->getErrorMessage($errors));
		}
	}

	/**
	 * @param \DOMDocument $xml
	 * @return World
	 */
	private function createWorld(\DOMDocument $xml) {
		$simpleXml = simplexml_import_dom($xml);
		if ($simpleXml === false) {
			throw new DeserializationException("Unable to instantiate SimpleXMLElement");
		}
		$size = intval($simpleXml->world->cells);
		$speciesCount = intval($simpleXml->world->species);
		$iterationsCount = intval($simpleXml->world->iterations);

		$this->builder->newWorld($size, $size, $speciesCount, $iterationsCount);

		foreach ($simpleXml->organisms->organism as $organism) {
			$x = intval($organism->x_pos);
			$y = intval($organism->y_pos);
			$species = intval($organism->species);
			try {
				$this->builder->withOrganism($x, $y, $species);
			} catch (WorldException $we) {
				throw new DeserializationException("XML contains invalid data: " . $we->getMessage());
			}
		}

		return $this->builder->build();
	}

	private function getErrorMessage(array $errors) {
		$messages = [];
		foreach ($errors as $error) {
			$messages[] = "$error->message (line $error->line, column $error->column)";
		}
		return implode(", ", $messages);
	}


}