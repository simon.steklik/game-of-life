<?php
/**
 * User: simon
 */

namespace Gol\Serialization;

/**
 * Interface ConflictStrategy represents strategy used when resolving multiple organisms in one cell
 * during World initialization
 */
interface ConflictStrategy {

	/**
	 * Resolves which organism should "win" the cell
	 * @param int[] $species
	 * @return int
	 */
	public function resolveConflict(array $species);
}