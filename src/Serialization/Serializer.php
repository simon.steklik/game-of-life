<?php
/**
 * User: simon
 */

namespace Gol\Serialization;

use Gol\World\World;


/**
 * Class XmlSerializer is used for serializing and deserializing World instances to/from a string representation
 */
interface Serializer {
	/**
	 * @param World $world
	 * @return string
	 */
	public function serialize(World $world);

	/**
	 * @param string $stringRepresentation
	 * @return World
	 */
	public function deserialize($stringRepresentation);
}