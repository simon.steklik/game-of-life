<?php
/**
 * User: simon
 */

namespace Gol\Serialization;


class ValidationSchema {

	public static function get() {
		$schema = @file_get_contents(__DIR__ . '/../../resources/validationSchema.xsd');
		if ($schema === false) {
			$error = error_get_last();
			throw new \Exception("Cannot read validation schema file, " . ($error === null ? 'unknown error' : $error['message']) . "\n");
		}
		return $schema;
	}
}