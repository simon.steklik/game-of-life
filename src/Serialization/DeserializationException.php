<?php
/**
 * User: simon
 */

namespace Gol\Serialization;

/**
 * Class DeserializationException is thrown when deserialization fails
 */
class DeserializationException extends \RuntimeException {}