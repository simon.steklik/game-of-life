<?php
/**
 * User: simon
 */

namespace Gol;

/**
 * Class GolException represents a general GameOfLife related error
 */
class GolException extends \Exception {}